(function () {
  // 不在手机上调试可将VConsole注释
  // new VConsole();
  Toast.init();

  const oTips = $(".tips");
  const oBrowserTips = $(".browser-tips");
  const oButtonDownload = $(".download-button");
  const oButtonOpenApp = $(".open-tips");
  const oSwiper = $(".app-swiper");

  const referralCode = getQuery("referralCode");
  const env = getEnv();
  let isAnimating = false;
  let downloadUrl = "";
  console.log("referralCode", referralCode);
  console.log("currEnv", env);

  // 初始化轮播图
  const swiper = new Swiper(".app-swiper", {
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    pagination: {
      el: ".swiper-pagination",
    },
  });

  var player = videojs('my-video');
  player.on('play', function() {
    swiper.autoplay.pause()
  });
  player.on('ended', function() {
    swiper.autoplay.resume()
  });

  // 控制默认显示
  switch (env) {
    // 微信环境，提示浏览器打开
    case "weixin":
      oTips.classList.add("show");
      break;
    // iOS环境
    case "ios":
      oButtonOpenApp.classList.add("show");
      break;
    // Android环境
    case "android":
      oButtonOpenApp.classList.add("show");
      break;
    case "unknown":
      oBrowserTips.classList.add("show");
      break;
  }

  // 获取下载链接
  request(kURI_FOR_DOWNLOAD, {
    headers: {
      clientType: env,
    },
    success: function (resp) {
      downloadUrl = resp.data.link;
    },
    fail: function (errMsg) {
      Toast.info(errMsg);
    },
  });

  oSwiper.onmouseenter = function () {
    swiper.autoplay.pause()
  }
  oSwiper.onmouseleave = function () {
    swiper.autoplay.resume()
  }

  // 唤醒APP
  oButtonOpenApp.onclick = function () {
    if (["ios", "android"].indexOf(env) !== -1) {
      window.location.href = kSCHEME_URI;
    }
  };

  // 下载APP
  oButtonDownload.onclick = function () {
    if (env === "weixin") {
      // -- 微信环境
      if (isAnimating) {
        return;
      }
      isAnimating = true;
      oTips.classList.add("ani");
      const t = setTimeout(function () {
        isAnimating = false;
        oTips.classList.remove("ani");
        clearTimeout(t);
      }, 1000);
    } else {
      // -- 应用环境
      if (downloadUrl) {
        // 1. 直接跳转
        // window.location.href = downloadUrl;

        // 2. 需要注册
        path += "?downloadUrl=" + encodeURIComponent(downloadUrl);
        if (referralCode) {
          path += "&referralCode=" + referralCode;
        }
        window.location.href = path;
      } else {
        Toast.info("Not yet open, stay tuned！");
      }
    }
  };
})();
